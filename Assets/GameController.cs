﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameController : MonoBehaviour 
{
    public static GameController instance;
    public Text endText;


    private void Start()
    {
        instance = this;
    }

    public void Loose()
    {
        endText.text = "Ты проиграл";
        endText.color = Color.red;
        StartCoroutine(Restart());
    }

    public void Win()
    {
        endText.text = "Ты победил";
        endText.color = Color.green;
        StartCoroutine(Restart());
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
