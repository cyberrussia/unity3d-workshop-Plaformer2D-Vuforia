﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemyController : MonoBehaviour 
{
    public Transform leftPoint;
    public Transform rightPoint;

    public float moveSpeed = 5;
    public int direction = 1;

    private Rigidbody2D myRigidbody;

    public bool movingRight;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (movingRight && transform.position.x > rightPoint.position.x)
        {
            movingRight = false;
        }
        if (!movingRight && transform.position.x < leftPoint.position.x)
        {
            movingRight = true;
        }

        direction = movingRight ? 1 : -1;

        myRigidbody.velocity = new Vector3(moveSpeed * direction, myRigidbody.velocity.y, 0);
        transform.localScale = new Vector3(-direction, transform.localScale.y, transform.localScale.z);
    }

}
