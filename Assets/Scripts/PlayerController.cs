﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    public float speed = 5;
    public float jumpSpeed = 6;
    public Transform groundChecker;
    public float groundCheckRadius = 0.3f;
    public LayerMask groundCheckLayer;
    public int direction = 1;
    public GameObject deathExplosion;
    public float deathExplosionTime = 1;
    public GameObject bulletPrefab;
    public Transform firePoint;
    public float bulletSpeed = 10;


    private Rigidbody2D myRigidbody;
    private Animator myAnimator;
    public bool isGrounded;
   

	void Start ()
	{
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();

    }
	
	void Update ()
	{
        isGrounded = Physics2D.OverlapCircle(groundChecker.position, groundCheckRadius, groundCheckLayer);

        float moveHorizontal = Input.GetAxis("Horizontal");
        myRigidbody.velocity = new Vector2(moveHorizontal * speed, myRigidbody.velocity.y);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpSpeed);
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }

        if (moveHorizontal > 0)
        {
            direction = 1;
        }
        if (moveHorizontal < 0)
        {
            direction = -1;
        }

        transform.localScale = new Vector3(direction, 1, 1);


        myAnimator.SetFloat("Speed", Mathf.Abs(moveHorizontal));
        myAnimator.SetBool("Grounded", isGrounded);


    }

    private void Fire()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(bulletSpeed * direction, 0);
        bullet.transform.localScale = new Vector3(direction, 1, 1);
        Destroy(bullet, 5f);

        myAnimator.SetTrigger("Fired");
    }

    public void Die()
    {
        gameObject.SetActive(false);
        GameObject explosion = Instantiate(deathExplosion, transform.position, transform.rotation);
        Destroy(explosion, deathExplosionTime);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("KillPlane"))
        {
            GameController.instance.Loose();
            Die();
        }
    }
}
