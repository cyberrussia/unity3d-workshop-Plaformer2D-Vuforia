﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
    public GameObject cameraTarget;

	void Start ()
	{
        cameraTarget = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update ()
	{
        transform.position = new Vector3(cameraTarget.transform.position.x, transform.position.y, transform.position.z);
	}
}
